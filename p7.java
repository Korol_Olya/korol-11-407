import java.util.*;

public class p7 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Print starting value: ");
		int x = sc.nextInt();
		System.out.print("Print ending value: ");
		int y = sc.nextInt();
		
		int max=0;
		int length=0;
		
		System.out.print(x + " -> ");
		
		
		do {
			if(x%2==0) {
				x/=2;
			}
			else {
				x=x*3+1;
			}
			length++;
			if(max<x) {
				max = x;
			}
			System.out.print(x + " ");
		} while(x!=1);
		
		if(y==1 || y==4 || y==2) {
			System.out.println("\nLength is Infinity and max value is " + max);
			System.out.println("If ending value is 4, 2 or 1, then length of calculates will be infinity.");
		} else {
			System.out.println("\nLength is " + length + " and max value is " + max);
		}
	}
}