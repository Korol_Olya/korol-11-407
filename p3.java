import java.util.*;

public class p3 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int a, b, ans = 1;
		System.out.print("Print number: ");
		a = sc.nextInt();
		System.out.print("Print degree: ");
		b = sc.nextInt();
		
		if(b%2==0) {
			a*=a;
			b/=2;
		}
		for(int i=0; i<b; ++i) {
			ans*=a;
		}
		
		System.out.println(a + " ^ " + b + " = " + ans);
	}
}