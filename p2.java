import java.util.*;

public class p2 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int a, b, ans = 0;
		System.out.print("Print first number: ");
		a = sc.nextInt();
		System.out.print("Print second number: ");
		b = sc.nextInt();
		
		int min_number = a;
		if(a>b) {
			min_number = b;
		}
		for(int i=1; i<min_number; ++i) {
			if(a%i==0 && b%i==0) {
				ans = i;
			}
		}
		
		System.out.println("NCF for " + a + " and " + b + " is " + ans);
	}
}