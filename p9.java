import java.util.*;

public class p9 {
	public static void main(String[] args) {
		final int n = 4;
		int[][] mass = new int [n][n];
		for(int x=0; x<n; ++x) {
			for(int y=0; y<n; ++y) {
				if(x>y) {
					mass[x][y] = n-y;
				}
				else {
					mass[x][y] = n-x;
				}
				System.out.print(mass[x][y]);
			}
			System.out.println();
		}
	}
}